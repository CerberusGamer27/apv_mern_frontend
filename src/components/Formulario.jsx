import { useState, React, useEffect } from 'react'
import Alerta from '../components/Alerta'
import usePacientes from '../hooks/usePacientes';


const Formulario = () => {

  const [nombre, setNombre] = useState('');
  const [propietario, setPropietario] = useState('');
  const [email, setEmail] = useState('');
  const [fecha, setFecha] = useState(new Date());
  const [sintomas, setSintomas] = useState('');
  const [id, setId] = useState(null);

  const [alerta, setAlerta] = useState({});

  const { guardarPaciente, paciente, resetPaciente } = usePacientes();


  useEffect(() => {
    if (paciente?.nombre) {
      const temp = new Date(paciente.fecha)
      const fechaCorregida = new Date(temp.getTime() + temp.getTimezoneOffset() * 60000).toISOString().split('T')[0]
      setNombre(paciente.nombre);
      setPropietario(paciente.propietario);
      setEmail(paciente.email);
      setFecha(fechaCorregida);
      setSintomas(paciente.sintomas);
      setId(paciente._id);

    }
  }, [paciente])

  const resetForm = () => {
    setNombre('');
    setPropietario('');
    setEmail('');
    setFecha(new Date());
    setSintomas('');
    setId(null);
    resetPaciente();
  }


  const handleSubmit = e => {
    e.preventDefault();

    if ([nombre, propietario, email, fecha, sintomas].includes('')) {
      setAlerta({
        msg: 'Todos los campos son obligatorios',
        error: true
      })
      return;
    }

    guardarPaciente({ id, nombre, propietario, email, fecha, sintomas });
    setAlerta({
      msg: 'Guardado Correctamente'
    });
    setNombre('');
    setPropietario('');
    setEmail('');
    setFecha('');
    setSintomas('');
    setId(null);
    resetPaciente();
  }

  const { msg } = alerta
  return (
    <>
      <h2 className='font-black text-3xl text-center'>Administrador de Pacientes</h2>
      <p className='text-xl mt-5 mb-10 text-center'>
        Añade tus  {''}
        <span className='text-indigo-600 font-bold'>Pacientes y Administralos</span>
      </p>


      <form action="" className='bg-white py-10 px-5 mb-10 lg:mb-5 shadow-md rounded-md'
        onSubmit={handleSubmit}>
        <div className='mb-5'>
          <label htmlFor='nombre' className='text-gray-700 uppercase font-bold'>
            Nombre Mascota
          </label>
          <input type="text" id='nombre' placeholder='Nombre de la Mascota'
            className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
            value={nombre}
            onChange={e => setNombre(e.target.value)} />
        </div>

        <div className='mb-5'>
          <label htmlFor='propietario' className='text-gray-700 uppercase font-bold'>
            Nombre del Propietario
          </label>
          <input type="text" id='propietario' placeholder='Nombre del Propietario'
            className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
            value={propietario}
            onChange={e => setPropietario(e.target.value)} />
        </div>

        <div className='mb-5'>
          <label htmlFor='email' className='text-gray-700 uppercase font-bold'>
            Email del Propietario
          </label>
          <input type="email" id='email' placeholder='Email del Propietario'
            className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
            value={email}
            onChange={e => setEmail(e.target.value)} />
        </div>

        <div className='mb-5'>
          <label htmlFor='fecha' className='text-gray-700 uppercase font-bold'>
            Fecha de Alta
          </label>
          <input type="date" id='fecha'
            className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
            value={fecha}
            onChange={e => setFecha(e.target.value)} />
        </div>

        <div className='mb-5'>
          <label htmlFor='sintomas' className='text-gray-700 uppercase font-bold'>
            Sintomas
          </label>
          <textarea id='sintomas' placeholder='Describe los sintomas de la Mascota'
            className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
            value={sintomas}
            onChange={e => setSintomas(e.target.value)} />
        </div>

        <input type="submit"
          className='bg-indigo-600 w-full text-white uppercase font-bold p-3 
        hover:bg-indigo-800 cursor-pointer transition-colors'
          value={id ? 'Guardar Paciente' : 'Agregar Paciente'} />
        <input type="button"
          className='bg-red-400 w-full text-white uppercase font-bold p-3 mt-5
        hover:bg-red-600 cursor-pointer transition-colors'
          value='Reset Formulario' onClick={() => resetForm()} />
      </form>
      {msg && <Alerta alerta={alerta} />}
    </>
  )
}

export default Formulario