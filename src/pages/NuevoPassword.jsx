import { React, useEffect, useState } from 'react'
import { useParams, Link } from 'react-router-dom';
import Alerta from '../components/Alerta';
import clienteAxios from '../config/axios';


const NuevoPassword = () => {
    const [password, setPassword] = useState('');
    const [alerta, setAlerta] = useState({});
    const [tokeValudo, setTokenValido] = useState(false);
    const [passwordModificado, setPasswordModificado] = useState(false);

    const params = useParams();
    const { token } = params
    console.log(params)

    useEffect(() => {
        const comprobarToken = async () => {
            try {
                await clienteAxios(`/veterinarios/olvide-password/${token}`)
                setAlerta({ msg: 'Coloca tu Nuevo Password', error: false })
                setTokenValido(true)
            } catch (error) {
                setAlerta({ msg: 'Hubo un error con el Enlace', error: true })
            }
        }
        comprobarToken();
    }, [])

    const handleSubmit = async (e) => {
        e.preventDefault();
        if(password.length <6) {
            setAlerta({
                msg: 'El password deber ser minimo de 6 caracteres',
                error: true
            })
            return;
        }
        try {
            const url = `/veterinarios/olvide-password/${token}`;
            const {data} = await clienteAxios.post(url, {
                password
            })
            setPasswordModificado(true)
            setAlerta({msg:data.msg})
        } catch (error) {
            setAlerta({
                msg: error.response.data.msg,
                error: true
            })
        }
    }

    const { msg } = alerta
    return (
        <>
            <div className=''>
                <h1 className='text-indigo-600 font-black text-6xl text-center md:text-left'>Recupera tu Acceso y no Pierdas
                    <span className='text-black'> tus Pacientes</span>
                </h1>
            </div>
            <div className='mt-20 md:mt-5 shadow-lg px-5 py-10 rounded-xl bg-white'>
                {msg && <Alerta
                    alerta={alerta}
                />}
                {tokeValudo && (
                    <form onSubmit={handleSubmit}>
                        <div className='my-5'>
                            <label
                                className='uppercase text-gray-600 block text-xl font-bold'>
                                NUEVO PASSWORD
                            </label>
                            {/*En React es importante colocar la / cuando una etiqueta en HTML no tiene cierre*/}
                            <input
                                type={'password'}
                                placeholder="Tu Nuevo Password"
                                className='border w-full p-3 mt-3 bg-gray-100 rounded-xl'
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                            />
                        </div>
                        <input
                            type={'submit'}
                            value='guardar nuevo password'
                            className='bg-indigo-700 w-full py-3 rounded-xl text-white uppercase font-bold
                        mt-5 hover:cursor-pointer hover:bg-indigo-800 md:w-auto p-10'
                        />
                    </form>
                )}

                {passwordModificado && (
                    <Link className='block text-center my-5 text-gray-500'
                    to='/'>¿Ya tienes una cuenta? Inicia Sesion</Link>
                )}

            </div>
        </>
    )
}

export default NuevoPassword