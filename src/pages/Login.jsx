import { React, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import useAuth from '../hooks/useAuth'
import Alerta from '../components/Alerta';
import clienteAxios from '../config/axios';
import { ClimbingBoxLoader, PulseLoader } from 'react-spinners';

function Login() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [alerta, setAlerta] = useState({});
    const [loadingInProgress, setLoading] = useState(false);

    const navigate = useNavigate();

    const { setAuth } = useAuth();

    const handleSubmit = async (e) => {
        setLoading(true)
        e.preventDefault();
        if ([email, password].includes('')) {
            setAlerta({
                msg: 'Todos los campos son obligatorios',
                error: true
            })
            return
        }

        try {
            const { data } = await clienteAxios.post(`/veterinarios/login`, {
                email,
                password
            });

            localStorage.setItem('apv_token', data.token);
            setAuth(data)
            setLoading(false)
            navigate('/admin')
        } catch (error) {
            setLoading(false)
            setAlerta({
                msg: error.response.data.msg,
                error: true
            })
        }
    }


    const { msg } = alerta;
    return (
        <>
            <div className=''>
                <h1 className='text-indigo-600 font-black text-6xl text-center md:text-left'>Inicia Sesion y Administra tus
                    <span className='text-black'> Pacientes</span>
                </h1>
            </div>
            <div className='mt-20 md:mt-5 shadow-lg px-5 py-10 rounded-xl bg-white'>
                {msg && <Alerta
                    alerta={alerta}
                />}
                {loadingInProgress ? (
                    <div className="flex justify-center items-center p-16">
                        <PulseLoader color="#36d7b7" />
                    </div>
                ): (
                    <form onSubmit={handleSubmit}>
                    <div className='my-5'>
                        <label
                            className='uppercase text-gray-600 block text-xl font-bold'>
                            Email
                        </label>
                        {/*En React es importante colocar la / cuando una etiqueta en HTML no tiene cierre*/}
                        <input
                            type={'email'}
                            placeholder="Email de Registro"
                            className='border w-full p-3 mt-3 bg-gray-100 rounded-xl'
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                        />
                    </div>
                    <div className='my-5'>
                        <label
                            className='uppercase text-gray-600 block text-xl font-bold'>
                            Password
                        </label>
                        {/*En React es importante colocar la / cuando una etiqueta en HTML no tiene cierre*/}
                        <input
                            type={'password'}
                            placeholder="Password"
                            className='border w-full p-3 mt-3 bg-gray-100 rounded-xl'
                            value={password}
                            onChange={e => setPassword(e.target.value)}
                        />
                    </div>
                    <input
                        type={'submit'}
                        value='Iniciar Sesion'
                        className='bg-indigo-700 w-full py-3 rounded-xl text-white uppercase font-bold
                        mt-5 hover:cursor-pointer hover:bg-indigo-800 md:w-auto p-10'
                    />
                </form>
                )}
                <nav className='mt-10 lg:flex lg:justify-between'>
                    <Link className='block text-center my-5 text-gray-500'
                        to='/registrar'>¿No tienes una cuenta? Registrate</Link>
                    <Link className='block text-center my-5 text-gray-500'
                        to='/olvide-password'>Olvide mi Password</Link>
                </nav>
            </div>
        </>
    )
}

export default Login