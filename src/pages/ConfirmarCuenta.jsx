import { React, useState, useEffect } from 'react'
import { Link, useParams } from 'react-router-dom'
import Alerta from '../components/Alerta'
import clienteAxios from '../config/axios';

export default function ConfirmarCuenta() {
  // State para la alerta
  const [alerta, setAlerta] = useState({});
  const [cuentaConfirmada, setCuentaConfirmada] = useState(false);
  // El cargando nos servira para esperar qie es
  const [cargando, setCargando] = useState(true);
  // Hook que nos permite obtener los parametros de la URL de reac-router-dom
  const params = useParams();
  const { id } = params;
  
  // useEffect
  useEffect(() => {
    const confirmarCuenta = async () => {
      try {
        const url = `/veterinarios/confirmar/${id}`
        const { data } = await clienteAxios(url)
        setCuentaConfirmada(true);
        setAlerta({
          msg: data.msg,
          error: false
        })
      } catch (error) {
        setAlerta({
          msg: error.response.data.msg,
          error: true
        })
      }
      setCargando(false)
    }
    confirmarCuenta()
  }, [])

  return (
    <>
      <div>
        <h1 className='text-indigo-600 font-black text-6xl text-center md:text-left'>Confirma tu cuenta y empieza a Administrar
          <span className='text-black'> tus Pacientes</span>
        </h1>
      </div>
      <div className='mt-20 md:mt-5 shadow-lg px-5 py-10 rounded-xl bg-white'>
        {!cargando && <Alerta
          alerta={alerta}
        />}
        {cuentaConfirmada && 
          <Link className='block text-center my-5 text-gray-500'
              to='/'>Inicia Sesion</Link>}
      </div>
    </>
  )
}
