import { createContext, useState, useEffect } from "react";
import clienteAxios from '../config/axios'
import useAuth from "../hooks/useAuth";

const PacientesContext = createContext()


export const PacientesProvider = ({ children }) => {
    const { auth } = useAuth()
    const [pacientes, setPacientes] = useState([]);
    const [paciente, setPaciente] = useState({});

    useEffect(() => {

        const obtenerPaciente = async () => {
            try {
                const token = localStorage.getItem('apv_token');
                if (!token) return

                const config = {
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${token}`
                    }
                }

                const { data } = await clienteAxios('/pacientes', config);
                setPacientes(data);
            } catch (error) {
                console.error(error)
            }
        }

        obtenerPaciente();
    }, [auth])

    const resetPacientes = () => {
        setPacientes([]);
        setPaciente({});
    }

    const resetPaciente = () => {
        setPaciente({});
    }

    const guardarPaciente = async (paciente) => {
        const token = localStorage.getItem('apv_token');
        const config = {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            }
        }
        if (paciente.id) {
            try {
                const { data } = await clienteAxios.put(`/pacientes/${paciente.id}`, paciente, config);

                const pacienteActualizado = pacientes.map(pacienteState => pacienteState._id ===
                    data._id ? data : pacienteState);
                setPacientes(pacienteActualizado);
            } catch (error) {
                console.error(error)
            }
        } else {
            try {

                const { data } = await clienteAxios.post('/pacientes', paciente, config)
                // This delete the attributes of data, and create a new object called pacienteAlmacenado
                const { createdAt, updatedAt, __v, ...pacienteAlmacenado } = data;
                setPacientes([pacienteAlmacenado, ...pacientes]);
            } catch (error) {
                console.log(error.response.data.msg);

            }
        }
    }

    const setEdicion = (paciente) => {
        setPaciente(paciente)
    }

    const eliminarPaciente = async id => {
        const confirmar = confirm("Esta seguron que desea eliminar el Paciente?");
        if (confirmar) {
            try {
                const token = localStorage.getItem('apv_token');
                const config = {
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${token}`
                    }
                }

                const { data } = await clienteAxios.delete(`/pacientes/${id}`, config);
                console.log(data);
                const pacienteActualizado = pacientes.filter(pacientesState => pacientesState._id !== id);
                setPacientes(pacienteActualizado);
            } catch (error) {
                console.log(error);
            }
        }
    }

    return (
        <PacientesContext.Provider
            value={{
                pacientes,
                guardarPaciente,
                setEdicion,
                paciente,
                eliminarPaciente,
                resetPacientes,
                resetPaciente
            }}
        >
            {children}
        </PacientesContext.Provider>
    )
}

export default PacientesContext